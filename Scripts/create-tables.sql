use muebleria;

-- Creación de tablas:

create table if not exists categoria(
	id int not null auto_increment primary key comment 'Identificador de las categorias',
    descripcion varchar(200) not null
);

create table if not exists producto(
	id int not null auto_increment primary key comment 'Identificador de los productos',
    nombre varchar(50) not null,
    descripcion varchar(150),
    precio int not null comment 'Precio unitario por producto',
    idcategoria int not null,
    
    foreign key (idcategoria) references categoria(id)
);

create table if not exists stock(
	idproducto int not null,
    cantidad int not null comment 'Cantidad de productos en stock',
	
    foreign key (idproducto) references producto(id) 
);

create table if not exists compra(
	id int not null auto_increment primary key comment 'Identificador de las compras',
	idproducto int not null,
    fecha date not null,
    precio_compra int not null comment 'Precio total de la compra de produtos',
    cantidad int not null comment 'Cantidad de productos comprados',
    
    foreign key (idproducto) references producto(id)
);

create table if not exists boleta(
	id int not null auto_increment primary key comment 'Identificador de las boletas',
    codigo varchar(20) not null,
    fecha date not null,
    precio int not null comment 'Precio total por los productos'
);

create table if not exists factura(
	id int not null auto_increment primary key comment 'Identificador de las facturas',
    codigo varchar(12) not null,
    fecha date not null,
    precio int not null comment 'Precio total por los productos',
    iva int not null
);

create table if not exists venta(
	id int not null auto_increment primary key comment 'Identificador de las ventas',
    idboleta int,
    idfactura int,
    
    foreign key (idboleta) references boleta(id),
    foreign key (idfactura) references factura(id)
);

create table detalleVenta(
	idventa int not null comment 'Identificador de una venta',
    idproducto int not null comment 'Identificador del producto de la venta',
	cantidad int not null comment 'Cantidad del producto vendido',
    
	foreign key (idventa) references venta(id),
    foreign key (idproducto) references producto(id)
);
