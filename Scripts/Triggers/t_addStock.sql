/* Trigger para cambiar la cantidad (agregar) de un producto en el stock.
 * Esta cantidad cambiara cada vez que se agregue una fila a la tabla compra.
 * declare:
 * newCantidad int : Variable que almacenara la nueva cantidad a cambiar, por defecto es 0. 
 */
delimiter $$
create trigger t_addStock
after insert on compra
for each row
begin
	declare newCantidad int default 0;
    
    select
		(cantidad + new.cantidad) into newCantidad
    from stock
    where idproducto = new.idproducto;
    
    update stock set
		cantidad = newCantidad
	where idproducto = new.idproducto;
end
$$