-- Implemetacion de metodos calls para inserts (datos de prueba).

-- Categorias
call sp_insertCategoria('Pantallas');
call sp_insertCategoria('Living');
call sp_insertCategoria('Sillones');
call sp_insertCategoria('Canastos');
call sp_insertCategoria('Adornos');
call sp_insertCategoria('Sillas');

-- Productos
call sp_insertProducto('Pantalla espinas', 'Pantalla tejida con formas de espinas, varilla cocida y barnizada.', 20000, 1);
call sp_insertProducto('Pantalla huevo', 'Panatalla con forma de huevo, varilla delgada natural.', 30000, 1);
call sp_insertProducto('Pantalla campana', 'Pantalla en forma de campana, varilla cocida sin barnizar', 25000, 1);

call sp_insertProducto('Juego de living cuadrado', 'Juegos de living con los sofas y sillones de forma cuadrada, varilla cocida y pintados en cafe.', 400000, 2);
call sp_insertProducto('Juego de living L', 'Juego de living con los sillones cuadrados y un sofa en forma de L, varilla natural y barnizado.', 550000, 2);
call sp_insertProducto('Juego de living redondo', 'Juego de living con los sofas y sillones de terminaciones redondeadas, varilla cocida y pintados en cafe claro.', 420000, 2);

call sp_insertProducto('Sillon redondo', 'Sillon con terminaciones redondas, base cuadrada, varilla cocida y barnizado.', 90000, 3);
call sp_insertProducto('Sillon huevo', 'Sillon colgado con forma de medio huevo, varilla natural completa.', 100000, 3);
call sp_insertProducto('Sofa cuadrado', 'Sofa con terminaciones cuadradas, varilla cocida y barnizado.', 120000, 3);

call sp_insertProducto('Canasto leñero', 'Canasto para leña, varilla cocida completa, con terminaciones de pajilla.', 40000, 4);
call sp_insertProducto('Canasto pintado', 'Canasto de varilla natural, pintado en azul y rojo.', 34500, 4);
call sp_insertProducto('Canasto picnic', 'Canasto para picnic, varilla cocida y barnizado.', 32000, 4);

call sp_insertProducto('Adorno estrella', 'Adorno para muralla con forma de estrella, varilla cocida sin barnizar.', 25000, 5);
call sp_insertProducto('Adorno redondo', 'Adorno para muralla con forma redonda, varilla cocida sin barnizar.', 22000, 5);
call sp_insertProducto('Adorno bota navidad', 'Adorno pequeño con forma de bota navideña, varilla cocida y barnizado.', 10000, 5);

call sp_insertProducto('Silla bar', 'Silla alta, semanjante a una silla de bar, patas metalicas, varilla cocida y barnizada.', 50000, 6);
call sp_insertProducto('Silla comedor', 'Silla para comedor, patas metalicas cubiertas, varilla natural y barnizado.', 45000, 6);
call sp_insertProducto('Silla mecedora', 'Silla mecedora completa de mimbre, base de madera, varilla cocida completa y barnizada.', 74000, 6);

-- Stock
call sp_insertStock(1, 100);
call sp_insertStock(2, 120);
call sp_insertStock(3, 200);
call sp_insertStock(4, 5);
call sp_insertStock(5, 4);
call sp_insertStock(6, 8);
call sp_insertStock(7, 90);
call sp_insertStock(8, 85);
call sp_insertStock(9, 103);
call sp_insertStock(10, 230);
call sp_insertStock(11, 187);
call sp_insertStock(12, 210);
call sp_insertStock(13, 40);
call sp_insertStock(14, 176);
call sp_insertStock(15, 200);
call sp_insertStock(16, 87);
call sp_insertStock(17, 105);
call sp_insertStock(18, 60);

-- Compras
call sp_insertCompra(1, 50000, 10);
call sp_insertCompra(1, 120000, 15);

call sp_insertCompra(2, 60000, 10);
call sp_insertCompra(2, 60000, 10);

call sp_insertCompra(3, 45000, 8);
call sp_insertCompra(3, 40000, 10);

call sp_insertCompra(4, 600000, 2);
call sp_insertCompra(4, 350000, 1);

call sp_insertCompra(5, 400000, 1);
call sp_insertCompra(5, 900000, 2);

call sp_insertCompra(6, 1000000, 3);
call sp_insertCompra(6, 200000, 1);

call sp_insertCompra(7, 1000000, 20);
call sp_insertCompra(7, 100000, 2);

call sp_insertCompra(8, 65000, 1);
call sp_insertCompra(8, 195000, 3);

call sp_insertCompra(9, 200000, 2);
call sp_insertCompra(9, 350000, 4);

call sp_insertCompra(10, 800000, 40);
call sp_insertCompra(10, 380000, 20);

call sp_insertCompra(11, 600000, 30);
call sp_insertCompra(11, 1500000, 50);

call sp_insertCompra(12, 200000, 10);
call sp_insertCompra(12, 350000, 15);

call sp_insertCompra(13, 300000, 30);
call sp_insertCompra(13, 100000, 10);

call sp_insertCompra(14, 160000, 20);
call sp_insertCompra(14, 180000, 20);

call sp_insertCompra(15, 160000, 80);
call sp_insertCompra(15, 120000, 50);

call sp_insertCompra(16, 250000, 10);
call sp_insertCompra(16, 200000, 10);

call sp_insertCompra(17, 240000, 12);
call sp_insertCompra(17, 200000, 10);

call sp_insertCompra(18, 750000, 15);
call sp_insertCompra(18, 1000000, 20);

-- Boletas
call sp_insertBoleta('98523165478036527865', 70000);
call sp_insertBoleta('76543278154890936241', 55000);
call sp_insertBoleta('75231454892782523628', 400000);
call sp_insertBoleta('91786634326719877536', 970000);
call sp_insertBoleta('02735367422263885399', 420000);

-- Factura
call sp_insertFactura('163538297653', 180000, null);
call sp_insertFactura('758272451993', 270000, null);
call sp_insertFactura('213099356364', 220000, null);
call sp_insertFactura('002786343735', 160000, null);
call sp_insertFactura('882342435382', 164000, null);

-- Ventas
call sp_insertVenta(1, null);
call sp_insertVenta(2, null);
call sp_insertVenta(3, null);
call sp_insertVenta(4, null);
call sp_insertVenta(5, null);

call sp_insertVenta(null, 1);
call sp_insertVenta(null, 2);
call sp_insertVenta(null, 3);
call sp_insertVenta(null, 4);
call sp_insertVenta(null, 5);

-- Detalle ventas
call sp_insertDetalleVenta(1, 1, 2);
call sp_insertDetalleVenta(1, 2, 1);

call sp_insertDetalleVenta(2, 2, 1);
call sp_insertDetalleVenta(2, 3, 1);

call sp_insertDetalleVenta(3, 4, 1);

call sp_insertDetalleVenta(4, 4, 1);
call sp_insertDetalleVenta(4, 5, 1);

call sp_insertDetalleVenta(5, 6, 1);

call sp_insertDetalleVenta(6, 7, 2);

call sp_insertDetalleVenta(7, 7, 3);

call sp_insertDetalleVenta(8, 8, 1);
call sp_insertDetalleVenta(8, 9, 1);

call sp_insertDetalleVenta(9, 9, 1);
call sp_insertDetalleVenta(9, 10, 1);

call sp_insertDetalleVenta(10, 17, 1);
call sp_insertDetalleVenta(10, 18, 2);