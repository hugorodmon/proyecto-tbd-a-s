/* Sp para seleccionar solo los datos de una venta especifica y su boleta dada una id.
 * Estos datos estan dados por las tablas detalleVenta, venta, boleta y producto.
 * in:
 * _idventa int : Identificador de una venta en especifico.
 */
delimiter $$
create procedure sp_selectBoletaVenta(
	in _idventa int
)
begin
	select
		p.nombre as 'Nombre producto',
        dv.cantidad as 'Cantidad',
        b.codigo as 'Codigo boleta',
        b.fecha as 'Fecha boleta',
        b.precio as 'Precio en boleta'
	from detalleVenta dv
    left join producto p
		on dv.idproducto = p.id
	left join venta v
		on dv.idventa = v.id
	left join boleta b
		on v.idboleta = b.id
	where dv.idventa = _idventa;
end
$$