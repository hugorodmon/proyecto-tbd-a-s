/* Sp para seleccionar todos los detalles de una venta especifica dada una id.
 * Estos detalles estan dados por las tablas detalleVenta, venta, producto, boleta y factura.
 * in:
 * _idventa int : Identificador de una venta en especifico.
 */
delimiter $$
create procedure sp_selectVentaCompleta(
	in _idventa int
)
begin
	select
		p.nombre as 'Nombre producto',
        dv.cantidad as 'Cantidad',
        b.codigo as 'Codigo boleta',
        b.fecha as 'Fecha boleta',
        b.precio as 'Precio en boleta',
        f.codigo as 'Codigo factura',
        f.fecha as 'Fecha factura',
        f.precio as 'Precio en factura',
        f.iva as 'Iva en factura'
	from detalleVenta dv
    left join producto p
		on dv.idproducto = p.id
	left join venta v
		on dv.idventa = v.id
	left join boleta b
		on v.idboleta = b.id
	left join factura f
		on v.idfactura = f.id
	where dv.idventa = _idventa;
end
$$