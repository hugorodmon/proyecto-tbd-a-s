/* Sp para seleccionar una categoria dada una id.
 * in:
 * _id int : Identificador de una categoria en especifico.
 */
delimiter $$
create procedure sp_selectCategoria(
	in _id int
)
begin
	select
		descripcion as Descripcion
	from categoria
    where id = _id;
end
$$