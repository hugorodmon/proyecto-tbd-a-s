/* Sp para seleccionar todos los detalles de ventas entre dos fechas especificadas.
 * Estos detalles estan dados por las tablas detalleVenta, venta, producto, boleta y factura.
 * in:
 * _fecha1 date : Primera fecha para la seleccion, fecha mas antigua.
 * _fecha2 date : Segunda fecha para la seleccion, fecha mas nueva.
 */
delimiter $$
create procedure sp_selectVentaBetweenDate(
	in _fecha1 date,
    in _fecha2 date
)
begin
	select
		p.nombre as 'Nombre producto',
        dv.cantidad as 'Cantidad',
        b.codigo as 'Codigo boleta',
        b.fecha as 'Fecha boleta',
        b.precio as 'Precio en boleta',
        f.codigo as 'Codigo factura',
        f.fecha as 'Fecha factura',
        f.precio as 'Precio en factura',
        f.iva as 'Iva en factura'
	from detalleVenta dv
    left join producto p
		on dv.idproducto = p.id
	left join venta v
		on dv.idventa = v.id
	left join boleta b
		on v.idboleta = b.id
	left join factura f
		on v.idfactura = f.id
	where (b.fecha between _fecha1 and _fecha2) or (f.fecha between _fecha1 and _fecha2);
end
$$