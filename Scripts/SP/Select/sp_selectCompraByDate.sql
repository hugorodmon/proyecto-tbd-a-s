/* Sp para seleccionar una compra dada un fecha.
 * in:
 * _fecha date : Fecha especifica de una compra.
 */
delimiter $$
create procedure sp_selectCompraByDate(
	in _fecha date
)
begin
	select
		p.nombre as Nombre,
        c.fecha as Fecha,
        c.precio_compra as 'Precio compra',
        c.cantidad as Cantidad
	from compra c
    left join producto p
		on c.idproducto = p.id
	where c.fecha = _fecha;
end
$$