/* Sp para seleccionar un producto dada una id o un nombre.
 * in:
 * _id int : Identificador de un producto en especifico.
 * _nombre varchar(50) : Nombre de un producto en especifico.
 */
delimiter $$
create procedure sp_selectProducto(
	in _id int,
    in _nombre varchar(50)
)
begin
    select
		p.nombre as Nombre,
        p.descripcion as Descripcion,
        p.precio as Precio,
        c.descripcion as Categoria
	from producto p
    left join categoria c
		on p.idcategoria = c.id
	where p.id = _id or p.nombre = _nombre;
end
$$