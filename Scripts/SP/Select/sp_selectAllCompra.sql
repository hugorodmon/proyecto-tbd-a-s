/* Sp para seleccionar todas las entradas de la tabla compra (con nombre de producto).
 */
delimiter $$
create procedure sp_selectAllCompra()
begin
	select
		p.nombre as Nombre,
        c.fecha as Fecha,
        c.precio_compra as 'Precio compra',
        c.cantidad as Cantidad
	from compra c
    left join producto p
		on c.idproducto = p.id;
end
$$