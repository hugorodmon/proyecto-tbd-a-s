/* Sp para seleccionar una cantidad dada una id de producto.
 * in:
 * _idproducto int : Identificador de un producto en especifico.
 */
delimiter $$
create procedure sp_selectStock(
	in _idproducto int
)
begin
	select
		p.nombre as Nombre,
		s.cantidad as Cantidad
	from stock s
    left join producto p
		on s.idproducto = p.id
    where idproducto = _idproducto;
end
$$