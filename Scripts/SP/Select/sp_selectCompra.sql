/* Sp para seleccionar una compra en especifico dada una id.
 * in:
 * _id int : Identificador de una compra en especifico.
 */
delimiter $$
create procedure sp_selectCompra(
	in _id int
)
begin
	select
		p.nombre as Nombre,
        c.fecha as Fecha,
        c.precio_compra as 'Precio compra',
        c.cantidad as Cantidad
	from compra c
    left join producto p
		on c.idproducto = p.id
	where c.id = _id;
end
$$