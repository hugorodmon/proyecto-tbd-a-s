/* Sp para seleccionar todos los detalles de las ventas del ultimo mes (ultimos 30 dias).
 * Estos detalles estan dados por las tablas detalleVenta, venta, producto, boleta y factura.
 */
delimiter $$
create procedure sp_selectVentaLastMonth()
begin
	select
		p.nombre as 'Nombre producto',
        dv.cantidad as 'Cantidad',
        b.codigo as 'Codigo boleta',
        b.fecha as 'Fecha boleta',
        b.precio as 'Precio en boleta',
        f.codigo as 'Codigo factura',
        f.fecha as 'Fecha factura',
        f.precio as 'Precio en factura',
        f.iva as 'Iva en factura'
	from detalleVenta dv
    left join producto p
		on dv.idproducto = p.id
	left join venta v
		on dv.idventa = v.id
	left join boleta b
		on v.idboleta = b.id
	left join factura f
		on v.idfactura = f.id
	where (b.fecha between date_sub(curdate(), interval 30 day) and curdate()) or (f.fecha between date_sub(curdate(), interval 30 day) and curdate());
end
$$