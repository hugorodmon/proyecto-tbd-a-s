/* Sp para seleccionar todas las compras que se agregaron el ultimo mes (ultimos 30 dias).
 */
delimiter $$
create procedure sp_selectCompraLastMonth()
begin
	select
		p.nombre as Nombre,
        c.fecha as Fecha,
        c.precio_compra as 'Precio compra',
        c.cantidad as Cantidad
	from compra c
    left join producto p
		on c.idproducto = p.id
	where c.fecha between date_sub(curdate(), interval 30 day) and curdate();
end
$$