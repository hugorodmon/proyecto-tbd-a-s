/* Sp para seleccionar todas las entradas de la tabla producto.
 */
delimiter $$
create procedure sp_selectAllProducto()
begin
    select
		p.nombre as Nombre,
        p.descripcion as Descripcion,
        p.precio as Precio,
        c.descripcion as Categoria
	from producto p
    left join categoria c
		on p.idcategoria = c.id;
end
$$