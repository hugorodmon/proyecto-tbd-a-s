/* Sp para seleccionar una compra entre dos fechas especificas.
 * in:
 * _fecha1 date : Primera fecha para la seleccion, fecha mas antigua.
 * _fecha2 date : Segunda fecha para la seleccion, fecha mas nueva.
 */
delimiter $$
create procedure sp_selectCompraBetweenDate(
	in _fecha1 date,
    in _fecha2 date
)
begin
	select
		p.nombre as Nombre,
        c.fecha as Fecha,
        c.precio_compra as 'Precio compra',
        c.cantidad as Cantidad
	from compra c
    left join producto p
		on c.idproducto = p.id
	where c.fecha between _fecha1 and _fecha2;
end
$$