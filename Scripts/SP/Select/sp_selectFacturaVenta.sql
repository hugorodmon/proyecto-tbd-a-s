/* Sp para seleccionar solo los datos de una venta especifica y su factura dada una id.
 * Estos datos estan dados por las tablas detalleVenta, venta, factura y producto.
 * in:
 * _idventa int : Identificador de una venta en especifico.
 */
delimiter $$
create procedure sp_selectFacturaVenta(
	in _idventa int
)
begin
	select
		p.nombre as 'Nombre producto',
        dv.cantidad as 'Cantidad',
        f.codigo as 'Codigo factura',
        f.fecha as 'Fecha factura',
        f.precio as 'Precio en factura',
        f.iva as 'Iva en factura'
	from detalleVenta dv
    left join producto p
		on dv.idproducto = p.id
	left join venta v
		on dv.idventa = v.id
	left join factura f
		on v.idfactura = f.id
	where dv.idventa = _idventa;
end
$$