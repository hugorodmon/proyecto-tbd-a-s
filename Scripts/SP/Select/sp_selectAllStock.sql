/* Sp para seleccionar todas las entradas de la tabla stock (con nombre del producto).
 */
delimiter $$
create procedure sp_selectAllStock()
begin
	select
		p.nombre as Nombre,
		s.cantidad as Cantidad
	from stock s
    left join producto p
		on s.idproducto = p.id;
end
$$