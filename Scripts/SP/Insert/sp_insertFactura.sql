/* Sp para insertar datos en la tabla factura.
 * in:
 * _codigo varchar(12) : Codigo identificador de una factura.
 * _precio int : Precio de la boleta sin iva.
 * _iva float : Porcentaje del iva "nuevo" que se ingresa, si no se ingresa nada el iva por defecto es 0.19.
 * declare:
 * _fecha date : Fecha actual de la factura.
 * _pIva float : Porcentaje del iva que por defecto es 0.19.
 */
delimiter $$
create procedure sp_insertFactura(
	in _codigo varchar(12),
    in _precio int,
    in _iva float
)
begin
	declare _fecha date default curdate();
    declare _pIva float default 0.19;
    
    if _iva > 0 then
		insert into factura(codigo, fecha, precio, iva)
			values (_codigo, _fecha, _precio, (_precio * _iva));
	else
		insert into factura(codigo, fecha, precio, iva)
			values (_codigo, _fecha, _precio, (_precio * _pIva));
    end if;
end
$$