/* Sp para insertar datos a la tabla producto.
 * in:
 * _nombre varchar(50) : Nombre del producto.
 * _descripcion varchar(150) : Descripcion del producto.
 * _precio int : Precio base del producto (sin agregados).
 * _idcategoria int : Identificador de la categoria a la que pertenece el producto.
 */
delimiter $$
create procedure sp_insertProducto(
	in  _nombre varchar(50),
	in  _descripcion varchar(150),
	in  _precio int,
	in  _idcategoria int
)
begin 
	insert into producto(nombre, descripcion, precio, idcategoria)
		values (_nombre, _descripcion, _precio, _idcategoria);
end 
$$