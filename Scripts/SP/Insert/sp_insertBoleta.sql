/* Sp para insertar datos a la tabla boleta.
 * in:
 * _codigo varchar(20) : Codigo identificador de una boleta fisica.
 * _precio int : Precio de la venta puesto en la boleta.
 * declare:
 * _fecha date : Fecha actual de la boleta.
 */
delimiter $$
create procedure sp_insertBoleta(
	in _codigo varchar(20),
    in _precio int
)
begin
	declare _fecha date default curdate();
	
	insert into boleta(codigo, fecha, precio)
		values (_codigo, _fecha, _precio);
end
$$ 