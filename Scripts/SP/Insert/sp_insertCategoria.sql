/* Sp para ingresar datos a la tabla categoria.
 * in:
 * _descripcion varchar(200) : Descripción de la categoria.
 */
delimiter $$
create procedure sp_insertCategoria(
	in _descripcion varchar(200)
)
begin 
	insert into categoria(descripcion)
		values (_descripcion);
end
$$