/* Sp para insertar datos a la tabla stock.
 * in:
 * _idproducto int : Identificador de un producto.
 * _cantidad int : Cantidad de articulos por producto en stock.
 */
delimiter $$
create procedure sp_insertStock(
	in _idproducto int,
	in _cantidad int
)
begin
	insert into stock(idproducto, cantidad)
		values (_idproducto, _cantidad);
end
$$