/* Sp para insertar datos a la tabla detallesVenta.
 * in:
 * _idventa int : Identificador de una venta en especifico.
 * _idproducto int : Identificador de uno de los productos de la venta.
 * _cantidad int : Cantidad de articulos por producto.
 */
delimiter $$
create procedure sp_insertDetalleVenta(
	in _idventa int,
    in _idproducto int,
    in _cantidad int
)
begin 
	insert into detalleVenta(idventa, idproducto, cantidad)
		values (_idventa, _idproducto, _cantidad);
end
$$