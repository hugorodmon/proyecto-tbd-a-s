/* Sp para insertar datos en la tabla venta.
 * in:
 * _idboleta int : Identificador de una boleta relacionada a la venta (puede ser null).
 * _idfactura int : Identificador de una factura relacionada a la venta (puede ser null).
 */
delimiter $$
create procedure sp_insertVenta(
	in _idboleta int,
    in _idfactura int
)
begin 
	insert into venta(idboleta, idfactura)
		values (_idboleta, _idfactura);
end
$$