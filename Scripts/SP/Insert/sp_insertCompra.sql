/* Sp para insertar datos a la tabla compra.
 * in:
 * _idproducto int : Identificador de un producto.
 * _precio_compra int : Precio de la compra del producto (lo pagado).
 * _cantidad int : Cantidad de articulos por producto comprado.
 * declare:
 * _fecha date : La fecha actual de la compra.
 */
delimiter $$
create procedure sp_insertCompra(
	in _idproducto int,
	in _precio_compra int,
	in _cantidad int
)
begin 
	declare _fecha date default curdate();
	
	insert into compra(idproducto, precio_compra, fecha, cantidad)
		values (_idproducto, _precio_compra, _fecha, _cantidad);
end 
$$