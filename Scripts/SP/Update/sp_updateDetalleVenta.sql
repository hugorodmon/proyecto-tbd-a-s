/* Sp para actualizar los datos de la tabla detalleVenta.
 * in:
 * _idventa int : Identificador de una venta en especifico.
 * _idproducto int : Identificador nuevo de un producto (puede ser null).
 * _cantidad int : Nueva cantidad de articulos por producto (puede ser null).
 */
delimiter $$
create procedure sp_updateDetalleVenta(
	in _idventa int,
    in _idproducto int,
    in _cantidad int
)
begin
	if _idproducto is null then
		select idproducto into _idproducto from detalleVenta where idventa = _idventa;
	end if;
	if _cantidad is null then
		select cantidad into _cantidad from detalleVenta where idventa = _idventa;
	end if;

	update detalleVenta set
		idproducto = _idproducto,
        cantidad = _cantidad
	where idventa = _idventa;
end
$$