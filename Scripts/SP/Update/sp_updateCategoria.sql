/* Sp para actualizar datos de la tabla categoria.
 * in:
 * _id int : Identificador de una entrada en la tabla categoria.
 * _descripcion varchar(200) : Nueva descripcion de una categoria.
 */
delimiter $$ 
create procedure sp_updateCategoria(
	in _id int,
	in _descripcion varchar(200)
)
begin
	update categoria set 
		descripcion = _descripcion 
	where id = _id; 
end
$$