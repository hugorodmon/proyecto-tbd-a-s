/* Sp para actualizar la cantidad de un producto en stock.
 * in:
 * _idproducto int : Identificador de un producto en especifico.
 * _cantidad int : Nueva cantidad del producto.
 */
delimiter $$ 
create procedure sp_updateStock(
	in _idproducto int,
	in _cantidad int
)
begin
	update stock set 
		cantidad = _cantidad
	where idproducto = _idproducto;
end
$$