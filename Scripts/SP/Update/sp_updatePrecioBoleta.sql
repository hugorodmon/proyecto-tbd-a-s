/* Sp para actualizar el precio de una boleta.
 * in:
 * _id int : Identificador de una boleta en especifico.
 * _precio int : Nuevo precio en boleta.
 */
delimiter $$
create procedure sp_updatePrecioBoleta(
	in _id int,
	in _precio int
)
begin
	update boleta set
		precio = _precio
	where id = _id;
end
$$