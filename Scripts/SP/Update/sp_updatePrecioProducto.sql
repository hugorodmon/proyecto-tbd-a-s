/* Sp para actualizar el precio de uno de los productos.
 * in:
 * _id int : Identificador de un producto en especifico (puede ser null si se desconoce).
 * _nombre varchar(50) : Nombre de un producto en especifico (puede ser null si se desconoce).
 * _precio int : Nuevo precio a cambiar en el producto.
 */
delimiter $$ 
create procedure sp_updatePrecioProducto(
	in _id int,
	in _nombre varchar(50),
	in _precio int
)
begin
	update producto set
		precio = _precio
	where id = _id or nombre = _nombre;
end
$$