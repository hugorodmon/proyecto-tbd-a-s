/* Sp para actualizar el precio y la cantidad un producto de la tabla compra
 * in:
 * _id int : Identificador de una entrada en la tabla.
 * _precio_compra int : Nuevo precio del producto (puede ser null si no se requiere cambio de precio).
 * _cantidad int : Nueva cantidad del producto (puede ser null si no se requiere cambio de cantidad).
 */
delimiter $$ 
create procedure sp_updateCompra(
	in _id int,
	in _precio_compra int,
	in _cantidad int
)
begin
	if _precio_compra is null then
		select precio_compra into _precio_compra from compra where id = _id;
	end if;
	if _cantidad is null then
		select cantidad into _cantidad from compra where id = _id;
	end if;
    
	update compra set
		precio_compra = _precio_compra,
		cantidad = _cantidad
	where id = _id;
end
$$