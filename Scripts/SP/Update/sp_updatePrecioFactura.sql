/* Sp para actualizar el precio de una factura.
 * in:
 * _id int : Identificador de una factura en especifico.
 * _precio int : Nuevo precio en la factura.
 * _iva float : Nuevo porcentaje iva, si se requiere (puede ser null).
 * declare:
 * _pIva float : Variable que almacenara el porcentaje de iva que se cambiara en la factura.
 */
delimiter $$
create procedure sp_updatePrecioFactura(
	in _id int,
	in _precio int,
    in _iva float
)
begin
	declare _pIva float;
    
	if _iva > 0 then
		select _iva into _pIva;
	else
		select (iva / precio) into _pIva from factura where id = _id;
	end if;
	
	update factura set
		precio = _precio,
        iva = (_precio * _pIva)
	where id = _id;
end
$$