/* Sp para actualizar el precio de uno de los productos.
 * in:
 * _id int : Identificador de un producto en especifico.
 * _nombre varchar(50) : Nuevo nombre del producto.
 */
delimiter $$ 
create procedure sp_updateNombreProducto(
	in _id int,
	in _nombre varchar(50)
)
begin
	update producto set
		nombre = _nombre
	where id = _id;
end
$$