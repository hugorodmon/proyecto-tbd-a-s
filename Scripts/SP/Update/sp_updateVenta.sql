/* Sp para actualizar los datos de la tabla venta.
 * in:
 * _id int : Identificador de una venta en especifico.
 * _idboleta int : Identificador de una nueva boleta (puede ser null).
 * _idfactura int : Identificador de una nueva factura (puede ser null).
 */
delimiter $$
create procedure sp_updateVenta(
	in _id int,
	in _idboleta int,
    in _idfactura int
)
begin
	update venta set
		idboleta = _idboleta,
		idfactura = _idfactura
	where id = _id;
end
$$