-- Creación de la base de datos:

create database muebleria character set = 'utf8mb4' collate 'utf8mb4_spanish2_ci';

use muebleria;

-- Creación de usuarios:

create user 'jefe'@'localhost' identified by 'password-por-confirmar1';

create user 'trabajador'@'localhost' identified by 'password-por-confirmar2';

create user 'dev'@'localhost' identified by 'grupo05passWORD';

-- Entrega de privilegios:

grant all privileges on muebleria.* to 'dev'@'localhost';

-- User: Jefe
grant delete on muebleria.* to 'jefe'@'localhost';

grant insert on muebleria.* to 'jefe'@'localhost';

grant select on muebleria.* to 'jefe'@'localhost';

grant update on muebleria.* to 'jefe'@'localhost';

-- User: Trabajador
grant select on muebleria.* to 'trabajador'@'localhost';

grant delete on muebleria.stock to 'trabajador'@'localhost';
grant delete on muebleria.boleta to 'trabajador'@'localhost';
grant delete on muebleria.factura to 'trabajador'@'localhost';
grant delete on muebleria.venta to 'trabajador'@'localhost';
grant delete on muebleria.detalleVenta to 'trabajador'@'localhost';

grant update on muebleria.stock to 'trabajador'@'localhost';
grant update on muebleria.boleta to 'trabajador'@'localhost';
grant update on muebleria.factura to 'trabajador'@'localhost';
grant update on muebleria.detalleVenta to 'trabajador'@'localhost';